/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface;

import business.City;
import business.Community;
import business.CommunityDirectory;
import business.Family;
import business.FamilyDirectory;
import business.House;
import business.HouseDirectory;
import business.Patient;
import business.PatientDirectory;
import business.Person;
import business.PersonDirectory;
import business.VitalSignHistory;
import business.VitalSigns;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;
import logic.RandomGenerator;

/**
 *
 * @author Parth
 */
public class TestClass {

    public void ageVsBloodPressure()
    {
    
    double[] age = { 2, 3, 4, 5, 6, 8, 10, 11 };

  double[] bloodPressure = { 21.05, 23.51, 24.23, 27.71, 30.86, 45.85, 52.12, 55.98 };
  
//  System.out.println("Expected output from Excel: y = 9.4763 + 4.1939x");
  
  RegressionModel model = new LinearRegressionModel(age, bloodPressure);
  model.compute();
  double[] coefficients = model.getCoefficients();
  
  System.out.printf("Output from our code: y = %.4f + %.4fx", coefficients[0], coefficients[1]);

    }
            
public static void main(String h[])
{
int personCounter=0;
int communityCounter=0;
int houseCounter=0;
int familyCounter=0;

double totalScore=0;

    Random randomGenerator = new Random();
    RandomGenerator classGenerator= new RandomGenerator();

//    InputUser inputUser=new InputUser();
//    totalScore=inputUser.getFromUser();
    
    City city=new City();
    city.setName("Flamingham");
   
    
    CommunityDirectory communityDirectory1=new CommunityDirectory();

    communityDirectory1.setCommunityDirectory();
    Community commarr[]= new Community[10];
    House house[] = new House[10];
    Family family[]=new Family[10];
    Person person[]=new Person[10];
    Patient patient[]=new Patient[1000];
    VitalSigns vitals[]=new VitalSigns[1000];
    Patient patientTemp[]= new Patient[1000];
    
    System.out.println(RandomGenerator.randInt(1, 10));
    
  
    for (int i=0; i<10; ++i)
    {
        commarr[i] = new Community();
        commarr[i] = communityDirectory1.addCommunity();
        commarr[i].setCommunityName("CommunityName_" + i);
        commarr[i].setCommunityLocation("Location_" + RandomGenerator.randInt(1, 10)+i);
        commarr[i].setCommunityCleanlinessScore(RandomGenerator.randDouble(5, 10));
        commarr[i].setCommunityID(""+communityCounter);
        HouseDirectory houseDirectory = new HouseDirectory();
     communityCounter++;

        
        for(int j=0; j<10;++j)
        {
            house[j] = new House();
            house[j] = houseDirectory.addHouse(communityDirectory1, commarr[i]);
            house[j].setHouseAddress("House address_" + RandomGenerator.randInt(100, 200));
            house[j].setHouseNumber("Unit " + "00"+RandomGenerator.randInt(1, 10));
            house[j].setHouseID(""+houseCounter);
            houseCounter++;
            
            FamilyDirectory familyDirectory = new FamilyDirectory();
            for(int k=0; k<2;++k)
            {
                    family[k] = new Family();
                    family[k] = familyDirectory.addFamily(houseDirectory, house[j]);
                    
                    family[k].setLastName("LastName_"+RandomGenerator.randInt(1, 400));
                    family[k].setFamilyID(" "+familyCounter);
                    PersonDirectory personDirectory = new PersonDirectory();
                    PatientDirectory patientDirectory = new PatientDirectory();
                    VitalSignHistory vsh = new VitalSignHistory();
                    
                    familyCounter++;
                    for(int l=0; l<5;++l)
                    {
                      
                        
                        patient[l] = new Patient();
                        patient[l] = patientDirectory.addPatient(familyDirectory, family[k]);
                        patient[l].setFname("FirstName_"+classGenerator.generateString(randomGenerator, "SAPRAT", 5));
                        patient[l].setLastName(family[k].getLastName());
                        patient[l].setSocialNumber(personCounter);
                        patient[l].setMobNumber(""+RandomGenerator.randInt(1000000, 99999999));
                        patient[l].setAge(RandomGenerator.randInt(20, 80));
                        patient[l].setIncome(RandomGenerator.randInt(10000, 999999));
                        patient[l].setGender(RandomGenerator.generateGender());

                        patient[l].setMRN(RandomGenerator.randInt(100000, 999999));
                        patient[l].setIsDiabetic(RandomGenerator.getRandomBoolean());
                        patient[l].setIsInsured(RandomGenerator.getRandomBoolean());
                        patient[l].setIsSmoker(RandomGenerator.getRandomBoolean());
                        
                        for (int m=0;m<5;++m)
                        {
                            vitals[m] = new VitalSigns();
                            vitals[m] = vsh.addVitals(patient[l]);
                            vitals[m].setBloodPressure(RandomGenerator.randInt(100, 150));
                            vitals[m].setCholestrol(RandomGenerator.randInt(100,150));
                            vitals[m].setRespirationRate(RandomGenerator.randInt(80,100));
                        }

                        personCounter++;                
                    }

            }
        }

    }

//////////////
int yu=0;
    for (int i = 0; i < 10; i++) {
        


        for (int j = 0; j < 10; j++) {

            for (int k = 0; k < 2; k++) {
                for (int l = 0; l < 5; l++) {
                    
patientTemp[yu]=
communityDirectory1.getCommunityDirectory().get(i).getHouseDirectory().gethouseList().get(j).getFamilyList().get(k).getPatientlist().get(l);
yu++;
                }

            }
        }

    }



    System.out.println(communityDirectory1);

//report 1 city level
double cleanlinessScore=0;

int maleCount=0;
for(int q=0;q<10;q++)
{
      cleanlinessScore=(cleanlinessScore+ communityDirectory1.getCommunityDirectory().get(q).getCommunityCleanlinessScore());
}
     System.out.println("Average community cleanliness score of the city:  "+cleanlinessScore/10);
    
//report 2 city level

for(int w=0;w<1000;w++)
{
if(patientTemp[w].getGender()=="Male")
{
maleCount=maleCount+1;
}
}
    System.out.println("Number of male citizens in the city are: "+ maleCount);
    
//report 3 chances of heart attack



//report 1 community level
double maxCleanlinessScore=(double) communityDirectory1.getCommunityDirectory().get(0).getCommunityCleanlinessScore();
for(int k=1;k<10;++k)
{
if(communityDirectory1.getCommunityDirectory().get(k).getCommunityCleanlinessScore()>maxCleanlinessScore)
{
maxCleanlinessScore=communityDirectory1.getCommunityDirectory().get(k).getCommunityCleanlinessScore();
}
    
}
System.out.println("The highest community level cleanliness score is: "+ maxCleanlinessScore);


//report 2 community level
//int totalPopulation=

        
        int counterPopulation = 0;
        int communityPopulation=0;
for (int i = 0; i < 1000; i ++)
    if (patientTemp[i] != null)
        counterPopulation ++;
System.out.println(counterPopulation+ " is the total population");

for(int j=0;j<10;++j)
{
if(communityDirectory1.getCommunityDirectory().get(j)!=null)
    communityPopulation++;
}
System.out.println(communityPopulation+ " is the community population\n");

System.out.println("Average population per community is: "+ counterPopulation/communityCounter);

//report 3 Community Level

for(int u=0;u<10;u++)
{
System.out.println("Community ID  "+communityDirectory1.getCommunityDirectory().get(u).getCommunityID()+" with Location "+communityDirectory1.getCommunityDirectory().get(u).getCommunityLocation());
}



//Person
//Report 1
//Age is greater than 40
    
int population_greater_40 = 0;
//int communityPopulation=0;
for (int i = 0; i < 1000; i ++)
    if (patientTemp[i].getAge() > 40)
        population_greater_40 ++;
System.out.println("Total population greater than 40--->"+population_greater_40);

//House Level
//Report 1
//No. of Families in community 1 house 

Scanner input=new Scanner(System.in);
System.out.println("Enter Community ID--->");
String communityinput=input.nextLine();
System.out.println("Enter House ID--->");
String houseinput=input.nextLine();
int families_in_comm1_house1=
communityDirectory1.getCommunityDirectory().get(Integer.parseInt(communityinput))
        .getHouseDirectory().gethouseList().get(Integer.parseInt(houseinput)).getFamilyList().size();
String houseaddress = communityDirectory1.getCommunityDirectory().get(Integer.parseInt(communityinput))
        .getHouseDirectory().gethouseList().get(Integer.parseInt(houseinput)).getHouseAddress();
yu++;
System.out.println("No. of Families in community and house are---->"+families_in_comm1_house1);
System.out.println("House address of these families is---->"+houseaddress);

//Report2
//House Address of last house in the last community
String agegreaterthan40_in_comm1=
communityDirectory1.getCommunityDirectory().get(9)
        .getHouseDirectory().gethouseList().get(9).getHouseAddress();

System.out.println("House Address of last house in the last community---->"+agegreaterthan40_in_comm1);

//Report3
//Total Houses in the city
int totalhouses=0;
    for(int i=0;i<10;++i)
    {
        for(int j =0; j<10;j++)
        {
        //     for (int k=0;k<2;++k)
        //     {
        totalhouses= totalhouses+communityDirectory1.getCommunityDirectory().get(i)
        .getHouseDirectory().gethouseList().get(j).getFamilyList().size();
        //}
        }  

    }
    System.out.println("Total houses in the city---->"+totalhouses);
    
//Report3
//Total Houses in the city
double blood_pressure=0;
double cholestrol=0;
double respirationrate=0;
Scanner inputnew=new Scanner(System.in);
System.out.println("Enter Community ID--->");
String newcommunityinput=input.nextLine();
System.out.println("Enter House ID--->");
String newhouseinput=input.nextLine();
System.out.println("Enter Family ID--->");
String newfamilyinput=input.nextLine();
System.out.println("Enter Person ID--->");
String newpersoninput=input.nextLine();
    for(int i=0;i<10;++i)
    {
        for(int j =0; j<10;j++)
        {
             for (int k=0;k<2;++k)
             {
                 for(int l=0;l<5;++l)
                 {
                     for(int m=0;m<5;++m)
                             {
                                 
                             blood_pressure= communityDirectory1.getCommunityDirectory().get(Integer.parseInt(newcommunityinput))
                .getHouseDirectory().gethouseList().get(Integer.parseInt(newhouseinput)).getFamilyList().get(Integer.parseInt(newfamilyinput)).getPatientlist().get(Integer.parseInt(newpersoninput)).getVitalSign().get(m).getBloodPressure();
                             cholestrol=communityDirectory1.getCommunityDirectory().get(i)
                .getHouseDirectory().gethouseList().get(j).getFamilyList().get(k).getPatientlist().get(l).getVitalSign().get(m).getCholestrol();
                             respirationrate= communityDirectory1.getCommunityDirectory().get(i)
                .getHouseDirectory().gethouseList().get(j).getFamilyList().get(k).getPatientlist().get(l).getVitalSign().get(m).getRespirationRate();
                             }
                 }
            }
        }  

    }
    System.out.println("Average Vitals of the Person");
        System.out.println("Blood Pressure-->"+blood_pressure/5);
        System.out.println("Cholestrol-->"+cholestrol/5);
        System.out.println("Respiration Rate-->"+respirationrate/5);
}


}
